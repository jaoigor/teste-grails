import br.kroton.MetaclassHelper

class BootStrap {

    def init = { servletContext ->
        new MetaclassHelper()
    }
    def destroy = {
    }
}
