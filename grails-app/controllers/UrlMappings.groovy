class UrlMappings {

	static mappings = {

        "/$controller/$action?/$id?(.$format)?" {
        }

        "500"(view: '/error')
        "404"(view: '/404')

		"/vestibular-online"(controller: "provaOnline", action: "index")
	}
}