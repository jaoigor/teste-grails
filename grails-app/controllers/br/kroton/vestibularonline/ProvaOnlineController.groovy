package br.kroton.vestibularonline

import colaborar.domains.utils.JasperExportFormat
import colaborar.domains.utils.JasperReportDef
import colaborar.domains.*
import eu.bitwalker.useragentutils.DeviceType
import eu.bitwalker.useragentutils.UserAgent
import org.apache.commons.io.FileUtils
import grails.util.Environment
import java.awt.image.BufferedImage
import java.awt.Image
import net.sf.jasperreports.engine.JasperPrintManager
import javax.imageio.ImageIO
import grails.transaction.Transactional


class ProvaOnlineController {

    def assinaturaDigitalService
    def jasperService
    def vestibularOnlineService
    def temaService
    def mailService

    def index() { }

    def instrucoesProvaOnline() {
        def inscricaoId
        inscricaoId = params.id
        if (inscricaoId) {
            inscricaoId = inscricaoId.decode()
        }

        if (!inscricaoId){ //Quando redireciona por ter tentado sair da aba vem post sem estar encriptado
            inscricaoId = params.id
        }

        def inscricao = Inscricao.get(inscricaoId)

        if(!inscricao){
            flash.error = message(code:'provaOnlineController.inscricao.notFound.message')
            redirect action: 'error'
            return
        }

        def numeroChances = inscricao.getChancesRestantesProvaOnline()
        if(numeroChances <= 0){
            flash.error = message(code:'provaOnlineController.inscricao.numeroChances.message')
            redirect action: 'error'
            return
        }

        if(inscricao.isProvaOnlineFinalizada()) {
            flash.error = message(code:'provaOnlineController.inscricao.finalizada.message')
            redirect action: 'error'
            return
        }

        def inscricaoOpcao = InscricaoOpcao.findByInscricaoAndSequenciaAndCancelado(inscricao, 1, false)
        if(!inscricaoOpcao){
            flash.error = message(code:'provaOnlineController.inscricaoOpcao.notFound.message')
            redirect action: 'error'
            return
        }

        def poloMarca = PoloMarca.findByPoloAndCancelado(inscricaoOpcao.ofertaPolo.polo, false)
        if(!poloMarca){
            redirect action: 'error'
            return
        }

        // Salvar retorno quando a inscrição vem do PUC ou VESTBR, pois é preciso retornar.
        inscricao.urlRetornoProvaOnline = params.urlRetornoProvaOnline
        inscricao.save()

        render view: 'instrucoesProvaOnline', model: [inscricao: inscricao, inscricaoOpcao: inscricaoOpcao, poloMarca: poloMarca]
    }

    def aceiteProvaOnline(){
        def inscricaoId
        if (params.id) {
            inscricaoId = params.id.decode()
        }
        def inscricao = Inscricao.get(inscricaoId)
        if(!inscricao){
            flash.error = message(code:'provaOnlineController.inscricao.notFound.message')
            redirect action: 'error'
            return
        }
        
        def numeroChances = inscricao.getChancesRestantesProvaOnline()
        if(numeroChances <= 0){
            flash.error = message(code:'provaOnlineController.inscricao.numeroChances.message')
            redirect action: 'error'
            return
        }

        if(inscricao.isProvaOnlineFinalizada()) {
            flash.error = flash.error = message(code:'provaOnlineController.inscricao.finalizada.message')
            redirect action: 'error'
            return
        }

        def inscricaoOpcao = InscricaoOpcao.findByInscricaoAndSequenciaAndCancelado(inscricao, 1, false)
        if(!inscricaoOpcao){
            flash.error = message(code:'provaOnlineController.inscricaoOpcao.notFound.message')
            redirect action: 'error'
            return
        }

        def poloMarca = PoloMarca.findByPoloAndCancelado(inscricaoOpcao.ofertaPolo.polo, false)
        if(!poloMarca){
            flash.error = message(code:'provaOnlineController.poloMarca.notFound.message')
            redirect action: 'error'
            return
        }
        render view: 'aceiteProvaOnline', model: [inscricao: inscricao, inscricaoOpcao: inscricaoOpcao, poloMarca: poloMarca]
    }

    @Transactional
    def provaOnline(){
        def inscricaoId
        if (params.id) {
            inscricaoId = params.id.decode()
        }
        def inscricao = Inscricao.get(inscricaoId)
        if(!inscricao){
            flash.error = message(code:'provaOnlineController.inscricao.notFound.message')
            redirect action: 'error'
            return
        }

        def numeroChances = inscricao.getChancesRestantesProvaOnline()
        if(numeroChances <= 0){
            flash.error = message(code:'provaOnlineController.inscricao.numeroChances.message')
            redirect action: 'error'
            return
        }

        if(inscricao.isProvaOnlineFinalizada()) {
            flash.error = flash.error = message(code:'provaOnlineController.inscricao.finalizada.message')
            redirect action: 'error'
            return
        }

        def inscricaoOpcao = InscricaoOpcao.findByInscricaoAndSequenciaAndCancelado(inscricao, 1, false)
        if(!inscricaoOpcao){
            flash.error = message(code:'provaOnlineController.inscricaoOpcao.notFound.message')
            redirect action: 'error'
            return
        }

        def poloMarca = PoloMarca.findByPoloAndCancelado(inscricaoOpcao.ofertaPolo.polo, false)
        if(!poloMarca){
            flash.error = message(code:'provaOnlineController.poloMarca.notFound.message')
            redirect action: 'error'
            return
        }
        if(!params.aceiteTermoVestibularOnline){
            redirect controller: 'provaOnline', action: 'aceiteProvaOnline', params:[id: params.inscricaoId]
            return
        }

        def temaAleatorio = temaService.buscarTemaAleatorio(inscricao.id)
        if (!temaAleatorio) {
            flash.error = message(code:'provaOnlineController.temaAleatorio.notFound.message')
            redirect action: 'error'
            return
        }

        def inscricaoProvaOnline = InscricaoProvaOnline.get(vestibularOnlineService.salvarTermoAceite(inscricao, temaAleatorio))

        def imageDirectory = grailsApplication.mainContext.servletContext.getRealPath('reports/')
        
        File termo = new File("/nas2/${inscricao.vestibular.ano}${inscricao.vestibular.semestre}/vestibular/provaOnline/termo/${inscricao.id}/${inscricaoProvaOnline.id}.pdf")
        JasperReportDef reportDef = new JasperReportDef(name:"aceite_termo_prova_online.jasper", fileFormat:JasperExportFormat.PDF_FORMAT, parameters: [inscricaoProvaOnlineId: inscricaoProvaOnline.id, imageDirectory: imageDirectory]);
        FileUtils.writeByteArrayToFile(termo, assinaturaDigitalService.assinar('secretaria_matheus.pfx', '06833592900', jasperService.generateReport(reportDef).toByteArray(), "", "Londrina - PR", "Matheus Solek\n068.335.929-00\nSecretaria Acadêmica Setorial EaD Unopar - Termo assinado digitalmente\nValidação através do link: http://www.unopar.br/validacao\nData de emissão: ${new Date().format('dd/MM/yyyy')}\n\n *\"Consideram-se documentos públicos ou particulares, para todos os fins legais, os documentos eletrônicos de que trata esta Medida Provisória. (...) § 2o O disposto nesta Medida Provisória não obsta a utilização de outro meio de comprovação da autoria e integridade de documentos em forma eletrônica, inclusive os que utilizem certificados não emitidos pela ICP-Brasil, desde que admitido pelas partes como válido ou aceito pela pessoa a quem for oposto o documento.\"\nCertificado encaminhado ao email ${inscricao?.email} automaticamente após o aceite.", 1))
    
        try {
            vestibularOnlineService.updateCaminhoAceiteProvaOnline(inscricaoProvaOnline.id, termo.path)
        } catch(Exception e) {
            flash.error = e.message
            redirect action: 'error'
            return
        }  

        if (Environment.current == Environment.PRODUCTION) {
            mailService.sendMail {
                multipart true
                to inscricao?.email
                subject "Certificado de Aceite Digital - Prova Online"
                attachBytes "certificado_aceite_prova_online.pdf", "application/pdf", termo.getBytes()
                body("Certificado de Aceite Digital - Prova Online\nInscrição: ${inscricao.id}")
            }
        }

        render view: 'provaOnline', model: [inscricao: inscricao, inscricaoProvaOnline: inscricaoProvaOnline, tema: temaAleatorio, duracaoProvaOnline: ((inscricao.vestibular.duracaoProvaOnline?:60) * 60)]
    }

    def saveProvaOnline() {

        def inscricaoProvaOnline = InscricaoProvaOnline.get(params.inscricaoProvaOnlineId)

        def inscricao = inscricaoProvaOnline?.inscricao
        if(!inscricao){
            flash.error = message(code:'provaOnlineController.inscricao.notFound.message')
            redirect action: 'error'
            return
        }

        if(!inscricaoProvaOnline){
            flash.error = message(code:'provaOnlineController.inscricao.notFound.message')
            redirect action: 'error'
            return
        }
        
        if(inscricao.isProvaOnlineFinalizada()) {
            flash.error = flash.error = message(code:'provaOnlineController.inscricao.finalizada.message')
            redirect action: 'error'
            return
        }

        def inscricaoOpcao = InscricaoOpcao.findByInscricaoAndSequenciaAndCancelado(inscricao, 1, false)
        if(!inscricaoOpcao){
            flash.error = message(code:'provaOnlineController.inscricaoOpcao.notFound.message')
            redirect action: 'error'
            return
        }

        def poloMarca = PoloMarca.findByPoloAndCancelado(inscricaoOpcao.ofertaPolo.polo, false)
        if(!poloMarca){
            redirect action: 'error'
            return
        }

        if(!inscricao?.vestibularAusente && !inscricao?.tipoCandidato?.equals('O')){
            //trocar o ensalamento do presencial para o ensalamento online.
            vestibularOnlineService.changeVestibularSalaToOnline(inscricao, inscricaoOpcao.ofertaPolo.polo)
        }else if(inscricao?.tipoCandidato?.equals('O')){
            //realizar o ensalamento do online.
            vestibularOnlineService.verifyHasVestibularSalaOnline(inscricao, inscricaoOpcao.ofertaPolo.polo)
        }

        inscricaoProvaOnline.tituloProva = params.tituloProva
        if(params.textoProva.length() > 2500)
            inscricaoProvaOnline.textoProva = params.textoProva.substring(0,2500)
        else
            inscricaoProvaOnline.textoProva = params.textoProva
        inscricaoProvaOnline.dataFimProva = new Date()
        inscricaoProvaOnline.save(flush:true)

        File folder = new File("/nas2/${inscricaoProvaOnline.inscricao.vestibular.ano}${inscricaoProvaOnline.inscricao.vestibular.semestre}/vestibular/provaOnline/prova/${inscricaoProvaOnline.inscricao.id}/")
        folder.mkdirs()
        JasperReportDef reportDef = new JasperReportDef(name:"prova_online.jasper", fileFormat:JasperExportFormat.PDF_FORMAT, parameters: [inscricaoProvaOnlineId: inscricaoProvaOnline.id])
        jasperService.generateReport(reportDef)
        Image image = JasperPrintManager.printPageToImage(reportDef.jasperPrinter, 0, 2.0f)
        BufferedImage bufferedImage = (BufferedImage) image
        def fileProvaOnline = new File(folder, "${inscricaoProvaOnline.id}.jpg")
        ImageIO.write(bufferedImage, "jpg", fileProvaOnline)

        try {
            vestibularOnlineService.salvaArquivoColetor(inscricaoProvaOnline.id, fileProvaOnline.getAbsolutePath())
        } catch(Exception e) {
            flash.error = e.message
            redirect action: 'error'
            return
        } 

        render view: 'saveProvaOnline', model: [inscricao: inscricao, poloMarca: poloMarca]
    }

    def error() {
        render view: 'error'
    }

    def redirecionarRetornoAcompanhe() {
        def urlAcompanheMobile =  "/acompanhe-inscricao/" + params.id
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"))
        if(userAgent.getOperatingSystem().getDeviceType().equals(DeviceType.MOBILE)) {
            redirect(uri: urlAcompanheMobile)
        } else {
            Inscricao inscricao = Inscricao.get(params.id)
            Marca m = Marca.executeQuery("""
                select pm.marca from InscricaoOpcao io, PoloMarca pm                     
                 where 
                    pm.polo = io.ofertaPolo.polo
                    and io.sequencia = 1 
                    and io.cancelado = 'N'
                    and io.inscricao.id = :inscricaoId
            """, [inscricaoId: params.id])[0]

            /**
                http://vestibular.unime.edu.br/
                http://vestibular.faculdadepitagoras.com.br/
                http://vestibular.faculdadedemacapa.com.br/
                http://vestibular.unic.br/
                https://vestibular.unoparead.com.br/
             */

            if (inscricao.urlRetornoProvaOnline) {
                redirect uri: inscricao.urlRetornoProvaOnline
                return
            }else if (m.id == 1){
                def urlUnopar = Colaborar.get('url.retorno.marca.unopar')
                redirect uri: urlUnopar.valor + params.id.toString().encode()
                return
            }
            else if (m.id == 4){
                def urlPitagoras = Colaborar.get('url.retorno.marca.pitagoras')
                redirect uri: urlPitagoras.valor + params.id.toString().encode()
                return
            }
            else if (m.id == 5){
                def urlUnic = Colaborar.get('url.retorno.marca.unic')
                redirect uri: urlUnic.valor + params.id.toString().encode()
                return
            }
            else if(m.id == 7) {
                def urlFama = Colaborar.get('url.retorno.marca.fama')
                redirect uri: urlFama.valor + params.id.toString().encode()
                return
            }
            else if(m.id == 8) {
                def urlUnime = Colaborar.get('url.retorno.marca.unime')
                redirect uri: urlUnime.valor + params.id.toString().encode()
                return
            }

            redirect controller: "vestibular", action: "dadosInscricao", id: params.id.toString().encode()
        }

    }

}