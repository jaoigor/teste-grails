package br.kroton.vestibularonline

import colaborar.domains.Inscricao
import colaborar.domains.InscricaoProvaOnline
import colaborar.domains.Coletor
import colaborar.domains.Polo
import colaborar.domains.Usuario
import colaborar.domains.Vestibular
import colaborar.domains.VestibularHorario
import colaborar.domains.VestibularHorarioPolo
import colaborar.domains.VestibularSala
import colaborar.domains.VestibularSalaVestibular

import grails.transaction.Transactional
import org.grails.web.util.WebUtils
import groovy.sql.*

@Transactional
class VestibularOnlineService {

    def sessionFactory
    def inscricaoService

    def salvarTermoAceite(def inscricao, def temaAleatorio) {
        updateDataAceiteTermoVestibularOnline(inscricao.id)
        def inscricaoProvaOnline = new InscricaoProvaOnline()
        inscricaoProvaOnline.inscricao = inscricao
        inscricaoProvaOnline.tema = temaAleatorio
        inscricaoProvaOnline.cancelado = false
        inscricaoProvaOnline.dataAceiteProva = new Date()
        inscricaoProvaOnline.dataInicioProva = new Date()
        inscricaoProvaOnline.ipAceite = WebUtils.retrieveGrailsWebRequest().getCurrentRequest().getRemoteAddr()
        if (inscricaoProvaOnline.validate()) {
            inscricaoProvaOnline.save(flush: true)
        }

        return inscricaoProvaOnline.id
    }

    def updateDataAceiteTermoVestibularOnline(String inscricaoId) {
        def inscricao = Inscricao.findById(inscricaoId)
        inscricao.dataAceiteVestibularOnline = new Date()
        inscricao.save()
    }

    def updateCaminhoAceiteProvaOnline(Long inscricaoProvaOnlineId, String caminhoAceiteProva) {
        def inscricaoProvaOnline = InscricaoProvaOnline.findById(inscricaoProvaOnlineId)
        inscricaoProvaOnline.caminhoAceiteProva = caminhoAceiteProva
        if(!inscricaoProvaOnline.save()) {
            throw new Exception("Não foi possível atualizar o termo de aceite, volte e tente novamente.")         
        }
    }

    def salvaArquivoColetor(Long inscricaoProvaOnlineId, String pathArquivo) {
        def inscricaoProvaOnline = InscricaoProvaOnline.get(inscricaoProvaOnlineId)
        def coletor = new Coletor()
        coletor.codigoBarra = getI2OF5(inscricaoProvaOnline.inscricao.id, "34")
        coletor.data = new Date()
        coletor.arquivo = "\\\\ead-nas2" + pathArquivo
        coletor.valido = true
        coletor.processado = false
        coletor.ip = WebUtils.retrieveGrailsWebRequest().getCurrentRequest().getRemoteAddr()
        coletor.usuarioLog = Usuario.get(115882)

        if (!coletor.save()) {
            throw new Exception("Não foi possível gravar o arquivo no coletor.")            
        }
    }

    def getI2OF5(String inscricaoId, String type) {
        def sql = new Sql(sessionFactory.getCurrentSession().connection())
        def result
        def length = 14
        sql.call("{? = call GETI2OF5(?,?)}", [Sql.VARCHAR, inscricaoId+type, length]) { r ->
            result = r
        }
        result
    }

    def changeVestibularSalaToOnline(Inscricao inscricao, Polo polo){
        if(inscricao.tipoCandidato.equals('O')){
            return false
        }

        Vestibular vestibular = inscricao.vestibular
        VestibularHorarioPolo horario
        Usuario usuario = Usuario.get(1)
        def vestibularHorario = VestibularHorario.executeQuery(""" from VestibularHorario vestibularHorario
                                                                           where vestibularHorario.vestibular.id = :vestibularId
                                                                           and vestibularHorario.horarioInicio = :inicioInscricaoInternet
                                                                           and vestibularHorario.horarioFim = :fimInscricaoInternet """,
                                                                           [vestibularId: vestibular.id, inicioInscricaoInternet: vestibular.dataInicioInscricaoInternet,
                                                                           fimInscricaoInternet: vestibular.dataFimInscricaoInternet])

        if(!vestibularHorario){

            vestibularHorario = inscricaoService.generateVestibularHorarioOnline(vestibular, 1, usuario)

            horario = inscricaoService.generateVestibularHorarioPoloOnline(vestibularHorario, polo, vestibular, usuario)

        } else {
            horario = VestibularHorarioPolo.findByVestibularHorarioAndPolo(vestibularHorario, polo)

            if(!horario){
                horario = inscricaoService.generateVestibularHorarioPoloOnline(vestibularHorario[0], polo, vestibular, usuario)
            }
        }


        inscricao.tipoCandidato = 'O'
        inscricao.vestibularHorarioPolo = horario
        inscricao.save(flush: true)

        return verifyHasVestibularSalaOnline(inscricao, polo)
    }

    def verifyHasVestibularSalaOnline(Inscricao inscricao, Polo polo){

        VestibularSalaVestibular vestibularSalaVestibular = VestibularSalaVestibular.findByVestibularHorarioPolo(inscricao.vestibularHorarioPolo)

        if(vestibularSalaVestibular){
            inscricao.vestibularSala = vestibularSalaVestibular.vestibularSala
            inscricao.save(flush: true)

            vestibularSalaVestibular.quantidade = vestibularSalaVestibular.quantidade + 1
            vestibularSalaVestibular.save(flush: true)
            return true
        }else{
            Usuario usuario = Usuario.get(1)
            def vestibularSala = VestibularSala.findByPoloAndDescricaoAndAtivo(polo, 'ONLINE', false)

            if(!vestibularSala){
                vestibularSala = generateVestibularSalaOnline(polo, usuario)
            }

            generateVestibularSalaOnlineVestibular(vestibularSala, inscricao.vestibularHorarioPolo, usuario)

            inscricao.vestibularSala = vestibularSala
            inscricao.save(flush: true)
        }
    }

    def generateVestibularSalaOnline(Polo polo, Usuario usuario){

        VestibularSala vestibularSala = new VestibularSala()
        vestibularSala.polo = polo
        vestibularSala.capacidade = 999
        vestibularSala.cancelada = false
        vestibularSala.ativo = false
        vestibularSala.descricao = 'ONLINE'
        vestibularSala.redistribuicao = false
        vestibularSala.usuarioLog = usuario
        vestibularSala.lastUpdated = new Date()
        vestibularSala.save(flush:true)

        return vestibularSala
    }

    def generateVestibularSalaOnlineVestibular(VestibularSala vestibularSala, VestibularHorarioPolo horarioPolo, Usuario usuario){

        VestibularSalaVestibular salaVestibular = new VestibularSalaVestibular()
        salaVestibular.vestibularSala = vestibularSala
        salaVestibular.vestibularHorarioPolo = horarioPolo
        salaVestibular.capacidade = 999
        salaVestibular.quantidade = 1
        salaVestibular.ocorrencia = false
        salaVestibular.usuarioLog = usuario
        salaVestibular.lastUpdated = new Date()
        salaVestibular.save(flush:true)
    }

}
