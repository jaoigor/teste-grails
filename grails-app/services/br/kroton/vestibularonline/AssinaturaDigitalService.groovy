package br.kroton.vestibularonline

import grails.transaction.Transactional

import java.security.KeyStore
import java.security.PrivateKey
import java.security.Security
import java.security.cert.Certificate

import org.bouncycastle.jce.provider.BouncyCastleProvider

import com.itextpdf.text.Font
import com.itextpdf.text.Rectangle
import com.itextpdf.text.Font.FontFamily
import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.PdfSignatureAppearance
import com.itextpdf.text.pdf.PdfStamper
import com.itextpdf.text.pdf.security.BouncyCastleDigest
import com.itextpdf.text.pdf.security.ExternalDigest
import com.itextpdf.text.pdf.security.ExternalSignature
import com.itextpdf.text.pdf.security.MakeSignature
import com.itextpdf.text.pdf.security.PrivateKeySignature
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard

@Transactional
class AssinaturaDigitalService {

  def grailsApplication

  def assinar(certificado, senha, bytes, motivo, local, assinatura, pageNumber) {
    Security.addProvider(new BouncyCastleProvider());
    def certificadoFolder = grailsApplication.parentContext.getResource("/certificados").file
    def certificadoFile = new File(certificadoFolder, certificado)

    KeyStore ks = KeyStore.getInstance("PKCS12")
    ks.load(new FileInputStream(certificadoFile), senha.toCharArray())
    String alias = ks.aliases().nextElement()
    PrivateKey pk = (PrivateKey)ks.getKey(alias, senha.toCharArray())
    Certificate[] chain = ks.getCertificateChain(alias)

    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    PdfReader reader = new PdfReader(bytes);
    PdfStamper stamper = PdfStamper.createSignature(reader, baos, '\0' as char)

    // Creating the appearance
    PdfSignatureAppearance appearance = stamper.getSignatureAppearance()
    appearance.setReason(motivo);
    appearance.setLocation(local);
    appearance.setVisibleSignature(new Rectangle(20, 20, 570, 200), pageNumber != null ? pageNumber : reader.getNumberOfPages(), "Assinatura")
    appearance.setLayer2Text(assinatura)
    appearance.setLayer2Font(new Font(FontFamily.COURIER, 8, Font.NORMAL))
    ExternalDigest digest = new BouncyCastleDigest()
    ExternalSignature es = new PrivateKeySignature(pk, "SHA-256", "BC")
    MakeSignature.signDetached(appearance, digest, es, chain, null, null, null, 0, CryptoStandard.CMS)
    baos.toByteArray()
  }
}
