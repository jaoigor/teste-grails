package br.kroton.vestibularonline

import colaborar.domains.Polo
import colaborar.domains.Usuario
import colaborar.domains.Vestibular
import colaborar.domains.VestibularHorario
import colaborar.domains.VestibularHorarioPolo
import grails.transaction.Transactional

@Transactional
class InscricaoService {

    @Transactional
    VestibularHorario generateVestibularHorarioOnline(Vestibular vestibular, Integer diaSemana, Usuario usuario){

        VestibularHorario vestibularHorario = populateVestibularHorario(vestibular, diaSemana, usuario)
        if(!vestibularHorario.save(flush:true, failOnError: true)){
            return null
        }

        return vestibularHorario

    }

    @Transactional
    VestibularHorarioPolo generateVestibularHorarioPoloOnline(VestibularHorario vestibularHorario, Polo polo, Vestibular vestibular, Usuario usuario){

        VestibularHorarioPolo vestibularHorarioPolo = populateVestibularHorarioPolo(vestibularHorario, polo, vestibular, usuario)


        if(!vestibularHorarioPolo.save(flush:true, failOnError: true)){
            return null
        }

        return vestibularHorarioPolo

    }

    VestibularHorarioPolo populateVestibularHorarioPolo(VestibularHorario vestibularHorario, Polo polo, Vestibular vestibular, Usuario usuario){

        VestibularHorarioPolo vestibularHorarioPolo = new VestibularHorarioPolo()

        vestibularHorarioPolo.vestibularHorario = vestibularHorario
        vestibularHorarioPolo.polo = polo
        vestibularHorarioPolo.dataProva = vestibular.dataInicioInscricaoInternet
        vestibularHorarioPolo.cancelado = false
        vestibularHorarioPolo.dataResultado = vestibular.dataInicioInscricaoInternet
        vestibularHorarioPolo.dataFimEnvio = vestibular.dataFimInscricaoInternet
        vestibularHorarioPolo.dataInicioMatricula = vestibular.dataInicioInscricaoInternet
        vestibularHorarioPolo.dataFimMatricula = vestibular.dataFimInscricaoInternet + 5
        vestibularHorarioPolo.versao = 0
        vestibularHorarioPolo.dataInicioImpressaoDocs = vestibular.dataInicioInscricaoInternet
        vestibularHorarioPolo.dataFimImpressaoDocs = vestibular.dataFimInscricaoInternet + 5
        vestibularHorarioPolo.dataInicioDivisaoSalas = vestibular.dataInicioInscricaoInternet
        vestibularHorarioPolo.dataFimDivisaoSalas = vestibular.dataInicioInscricaoInternet
        vestibularHorarioPolo.dataInicioImpressaoTermos = vestibular.dataInicioInscricaoInternet
        vestibularHorarioPolo.dataFimImpressaoTermos = vestibular.dataFimInscricaoInternet
        vestibularHorarioPolo.dataFimPne = vestibular.dataFimInscricaoInternet
        vestibularHorarioPolo.numeroMaximoInscricoes = 5000
        vestibularHorarioPolo.notificacao = false
        vestibularHorarioPolo.usuarioLog = usuario

        return vestibularHorarioPolo

    }

    VestibularHorario populateVestibularHorario(Vestibular vestibular, Integer diaSemana, Usuario usuario){

        VestibularHorario vestibularHorario = new VestibularHorario()

        vestibularHorario.vestibular = vestibular
        vestibularHorario.horarioInicio = vestibular.dataInicioInscricaoInternet
        vestibularHorario.horarioFim = vestibular.dataFimInscricaoInternet
        vestibularHorario.diaSemana = diaSemana
        vestibularHorario.usuarioLog = usuario

        return vestibularHorario

    }

}
