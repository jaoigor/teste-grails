package br.kroton.vestibularonline

import colaborar.domains.Inscricao
import colaborar.domains.OfertaPoloVestibular
import colaborar.domains.Vestibular

class VestibularService {

    def liberaProvaOnline(Inscricao inscricao, Vestibular vestibular, Date dataAtual, OfertaPoloVestibular ofertaPoloVestibular) {

        if (inscricao?.vestibularEnem) {
            return false
        }

        if (vestibular?.somenteAusenteOnline) {
            return false
        }

        if (dataAtual?.before(vestibular?.dataInicioOnline) || dataAtual?.after(vestibular?.dataFimOnline)) {
            return false
        }

        if (!ofertaPoloVestibular?.vestibularOnline) {
            return false
        }
        return true
    }
}