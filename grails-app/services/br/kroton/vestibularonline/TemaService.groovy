package br.kroton.vestibularonline

import colaborar.domains.Tema

class TemaService {

    def buscarTemaAleatorio(inscricaoId) {
        def tema = Tema.executeQuery("""select t from Inscricao i, VestibularTema vt, Tema t
                                   where vt.vestibular.id = i.vestibular.id
                                   and vt.cancelado = 'N'
                                   and t.id = vt.tema.id
                                   and i.id = :inscricaoId
                                   and t.temaOnline = 'S' 
                                   and t.cancelado = 'N'   
                                   and t.id not in(select ipv.tema.id from InscricaoProvaOnline ipv
                                       where ipv.cancelado = 'N'
                                             and ipv.inscricao.id = i.id)""", [inscricaoId: inscricaoId])[0]
        if(!tema){
            def temasUsados = Tema.executeQuery("""select ipv.tema from InscricaoProvaOnline ipv
                                   where ipv.inscricao.id = :inscricaoId
                                       and ipv.cancelado = 'N'
                                       and ipv.tema.temaOnline = 'S' 
                                       and ipv.tema.cancelado = 'N' """, [inscricaoId: inscricaoId])

            if(temasUsados.size() > 0)
                tema = temasUsados[new Random().nextInt(temasUsados.size())]
        }
        return tema
    }

}