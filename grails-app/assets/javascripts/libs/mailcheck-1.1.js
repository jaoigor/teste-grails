/*
 * Mailcheck https://github.com/Kicksend/mailcheck
 * Author
 * Derrick Ko (@derrickko)
 *
 * License
 * Copyright (c) 2012 Receivd, Inc.
 *
 * Licensed under the MIT License.
 *
 * v 1.1
 */

var Kicksend={mailcheck:{threshold:3,defaultDomains:["yahoo.com","google.com","hotmail.com","gmail.com","me.com","aol.com","mac.com","live.com","comcast.net","googlemail.com","msn.com","hotmail.co.uk","yahoo.co.uk","facebook.com","verizon.net","sbcglobal.net","att.net","gmx.com","mail.com"],defaultTopLevelDomains:["co.uk","com","net","org","info","edu","gov","mil"],run:function(n){n.domains=n.domains||Kicksend.mailcheck.defaultDomains,n.topLevelDomains=n.topLevelDomains||Kicksend.mailcheck.defaultTopLevelDomains,n.distanceFunction=n.distanceFunction||Kicksend.sift3Distance;var t=Kicksend.mailcheck.suggest(encodeURI(n.email),n.domains,n.topLevelDomains,n.distanceFunction);t?n.suggested&&n.suggested(t):n.empty&&n.empty()},suggest:function(n,t,i,r){var u,f,e,o;if(n=n.toLowerCase(),u=this.splitEmail(n),f=this.findClosestDomain(u.domain,t,r),f){if(f!=u.domain)return{address:u.address,domain:f,full:u.address+"@"+f}}else if(e=this.findClosestDomain(u.topLevelDomain,i),u.domain&&e&&e!=u.topLevelDomain)return o=u.domain,f=o.substring(0,o.lastIndexOf(u.topLevelDomain))+e,{address:u.address,domain:f,full:u.address+"@"+f};return!1},findClosestDomain:function(n,t,i){var u,e=99,f=null,r;if(!n||!t)return!1;for(i||(i=this.sift3Distance),r=0;r<t.length;r++){if(n===t[r])return n;u=i(n,t[r]),u<e&&(e=u,f=t[r])}return e<=this.threshold&&f!==null?f:!1},sift3Distance:function(n,t){var r;if(n==null||n.length===0)return t==null||t.length===0?0:t.length;if(t==null||t.length===0)return n.length;for(var i=0,u=0,f=0,e=0,o=5;i+u<n.length&&i+f<t.length;){if(n.charAt(i+u)==t.charAt(i+f))e++;else for(u=0,f=0,r=0;r<o;r++){if(i+r<n.length&&n.charAt(i+r)==t.charAt(i)){u=r;break}if(i+r<t.length&&n.charAt(i)==t.charAt(i+r)){f=r;break}}i++}return(n.length+t.length)/2-e},splitEmail:function(n){var u=n.split("@"),t;if(u.length<2)return!1;for(t=0;t<u.length;t++)if(u[t]==="")return!1;var f=u.pop(),i=f.split("."),r="";if(i.length==0)return!1;if(i.length==1)r=i[0];else{for(t=1;t<i.length;t++)r+=i[t]+".";i.length>=2&&(r=r.substring(0,r.length-1))}return{topLevelDomain:r,domain:f,address:u.join("@")}}}};window.jQuery&&function(n){n.fn.mailcheck=function(n){var r=this,i,t;n.suggested&&(i=n.suggested,n.suggested=function(n){i(r,n)}),n.empty&&(t=n.empty,n.empty=function(){t.call(null,r)}),n.email=this.val(),Kicksend.mailcheck.run(n)}}(jQuery);