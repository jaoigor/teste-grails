// user types for boolean (S/N)
grails.gorm.default.mapping = {
    'user-type'( type: br.unopar.domains.persistence.SNUserType, class: boolean )
    'user-type'( type: br.unopar.domains.persistence.SNUserType, class: Boolean )
}

environments {
    production {
        grails.assets.url = { request ->
                return "//vestibularonline.colaboraread.com.br/assets/"            
        }
    }
}