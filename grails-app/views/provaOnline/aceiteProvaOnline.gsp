<html>
<head>
    <meta name="layout" content="prova-online"/>
</head>

<body class="prova-online">
<main>

    <div class="center">
        <a href="#" class="logo-vestibulares-verde"></a>
    </div>

    <div class="container-prova bloco-principal">
        <div class="row-prova">
            <div class="col-2 col-12-sm center logo-apresentacao-prova">
                <g:if test="${!poloMarca.marca.id}">
                    <asset:image src="logos/logo_marca_kroton.png" alt="LOGO MARCA"/>
                </g:if>
                <g:else>
                    <asset:image src="logos/logo_marca_${poloMarca?.marca?.id}.gif" alt="LOGO MARCA"/>
                </g:else>
            </div>

            <div class="col-10 inicio-apresentacao">
                <p>
                    <strong>${inscricao.nome}</strong>, para ter acesso a prova você deverá realizar o aceite ao Termo de Responsabilidade que está disponibilizado abaixo:
                </p>
            </div>
        </div>
    </div>

    <div class="container-prova bloco-principal">
        <div class="row-prova">
            <div class="col-12">
                <h2 class="termoh2">Termo de responsabilidade de realização de prova on-line</h2>
            </div>
        </div>

        <div class="row-prova">
            <div class="col-12" id="aceiteScroll">
                <p>
                    Eu, <strong>${inscricao.nome}</strong>, inscrito (a) no <strong>${inscricao.vestibular.descricao}</strong> sob número de inscrição <strong>${inscricao.id}</strong>, declaro estar ciente e de acordo com as regras da Prova Online que irei realizar nesta data e horário <strong>${(new Date()).format('dd/MM/yyyy hh:mm:ss')}</strong>, para o curso <strong>${inscricaoOpcao.ofertaPolo.oferta.modulo.matriz.curso.nome}</strong> ofertado no polo <strong>${inscricaoOpcao.ofertaPolo.polo.nome}</strong>:<br/>
                    <br/>
                    1- A prova consiste em uma redação em Língua Portuguesa;<br/>
                    <br/>
                    2- O tempo de realização da prova é de ${inscricao?.vestibular?.duracaoProvaOnline} minutos, que serão controlados por meio de um cronômetro que encerrará a sua aplicação quando o tempo limite for atingido;<br/>
                    <br/>
                    3- O texto digitado será limitado em até 2.500 caracteres, que corresponde a aproximadamente 30 (trinta) linhas;<br/>
                    <br/>
                    4- Uma vez iniciada a prova não há como dar pausa, reiniciar ou retomar a mesma;<br/>
                    <br/>
                    5- Caso ocorra algum problema de conexão ou a prova seja fechada durante a execução, o candidato deverá acessar novamente a ferramenta através do link “Acompanhe sua Inscrição”, no máximo 5 vezes, e um novo tema será apresentado para elaboração de  uma nova redação;<br/>
                    <br/>
                    6- De acordo com a LEI Nº 12.853, de 14 de agosto de 2013, que dispõe sobre direitos autorais, alertamos sobre o cometimento de plágio.<br/>
                    <br/>
                    7- Se identificado plágio no texto digitado o mesmo será desconsiderado e a nota da redação será zero, desclassificando o candidato.
                </p>
            </div>
        </div>

        <div class="container bloco-links">
            <div class="row-prova center">

                <g:link controller="provaOnline" action="instrucoesProvaOnline" id="${inscricao.id.encode()}"
                        class="btn recusar">Recusar</g:link>
                <button id="liAceito" data-toggle="modal" data-target="#tentativasModal"
                        class="btn fill aceitaTermo disabled aceitar" disabled>Li e aceito os termos</button>
            </div>
        </div>
    </div>
</main>

<!-- MODAL TENTATIVAS -->
<div class="modal fade bd-example-modal-lg immutable" id="tentativasModal" tabindex="-1" role="dialog" aria-labelledby="modalTentativas" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div>
            <div class="modal-content">
                <div class="modal-header modal-header-sem-borda">
                    <h4 class="modal-title">Atenção à quantidade de tentativas!</h4>
                    <span id="regulamento-fechar-modal" data-dismiss="modal" aria-label="Close"></span>
                </div>
                <g:set var="quantidadeChancesRestantes" value="${inscricao.getChancesRestantesProvaOnline()}"/>
                <g:set var="quantidadeChancesEfetuadas" value="${(6 - quantidadeChancesRestantes)}"/>
                <g:form controller="provaOnline" action="provaOnline" class="js-init-prova">
                    <input type="text" name="id" value="${inscricao.id.encode()}" class="hidden"/>
                    <input type="checkbox" name="aceiteTermoVestibularOnline" checked class="hidden"/>
                    <div class="modal-body">
                        <g:if test="${quantidadeChancesRestantes <= 5 && quantidadeChancesRestantes > 1}">
                            <div class="row-prova" id="textoModal">
                                <div class="texto-aceita-termo">
                                    Essa é a sua <b>${(quantidadeChancesEfetuadas)}ª TENTATIVA</b> de fazer a Redação. Você só terá mais <b>${quantidadeChancesRestantes - 1} CHANCE(S)</b><br />
                                    se sair da Prova.
                                </div>
                                <div class="modal-footer center modal-footer-sem-borda center">
                                    <button type="submit" class="btn fill aceitaTermo">INICIAR REDAÇÃO</button>
                                </div>
                            </div>
                        </g:if>
                        <g:elseif test="${quantidadeChancesEfetuadas == 5}">
                            <div class="row" id="textoModal">
                                <div class="texto-aceita-termo">
                                    Essa é a sua <b>${quantidadeChancesEfetuadas}ª E ÚLTIMA TENTATIVA</b> de fazer a Prova Online. Você <b>NÃO PODERÁ</b> fazer um novo tema da Redação.
                                </div>
                            </div>
                            <div class="modal-footer modal-footer-sem-borda center">
                                <button type="submit" class="btn fill aceitaTermo">INICIAR REDAÇÃO</button>
                            </div>
                        </g:elseif>
                        <g:else>
                            <div class="row" id="textoModal">
                                <div class="texto-aceita-termo">
                                    Você esgotou <b>TODAS</b> as suas tentativas.
                                </div>
                            </div>
                        </g:else>
                    </div>
                </g:form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#liAceito').on('click', function () {
            $('#tentativasModal').modal('show');
            return false;
        })
    });
</script>

</body>
</html>