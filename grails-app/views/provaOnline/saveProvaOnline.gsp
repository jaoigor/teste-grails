<html>
	<head>
		<meta name="layout" content="prova-online" />
	</head>
	<body class="prova-online">
		<main>

			<div class="center">
				<a href="#" class="logo-vestibulares-verde"></a>
			</div>

			<div class="container-prova bloco-principal">
				<div class="row-prova">
					<div class="col-12 sm-center">
						<h2>Obrigado pela participação!</h2>
					</div>
				</div>
				<div class="row-prova save-prova-online sm-center">
					<div class="col-2 logo-apresentacao-prova">
						<g:if test="${!poloMarca.marca.id}">
							<asset:image src="logos/logo_marca_kroton.png" alt="LOGO MARCA"/>
						</g:if>
						<g:else>
							<asset:image src="logos/logo_marca_${poloMarca?.marca?.id}.gif" alt="LOGO MARCA"/>
						</g:else>
					</div>
					<div class="col-10 inicio-apresentacao">
						<p>
							<strong>${inscricao.nome}</strong>, em breve procederemos a correção de sua redação e em até 48 horas lhe retornaremos com o resultado por e-mail.
						</p>
					</div>
				</div>
				<div class="bloco-links">
					<div class="row-prova center">
						<g:link controller="provaOnline" action="redirecionarRetornoAcompanhe" id="${inscricao?.id}">Retorne para sua página principal</g:link>
					</div>
				</div>
			</div>
		</main>
	</body>
</html>