<html>
    <head>
        <meta name="layout" content="prova-online" />
    </head>
    <body class="prova-online">
        <g:set var="quantidadeChancesRestantes" value="${inscricao.getChancesRestantesProvaOnline()}"/>
        <main class="cbp-spmenu-push prova-topo-estatico">
            <div id="bg-menu-regulamento"></div>
            <g:form Id="save-prova-auto" controller="provaOnline" action="saveProvaOnline" class="js-init-prova" name="js-init-prova">
            <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                <span id="regulamento-fechar"></span>
                <h2>Regulamento:</h2>
                <p>Antes de iniciar a prova, leia as instruções abaixo com atenção:</p>
                <p>A prova consiste em uma redação em língua Portuguesa.</p>
                <p>A prova tem um cronômetro que controla o tempo e encerra a aplicação ao limite ser atingido;</p>
                <p>Uma vez iniciada a prova, não há como dar pausa, reiniciar ou retomar a mesma. Portanto, antes de começar, preste atenção na sua conexão e no ambiente ao seu redor, já que a execução não poderá ser interrompida;</p>
                <p>Para que você tenha um melhor desempenho, realize a prova em um local tranquilo, sem ruídos ou distrações;</p>
                <p>Caso aconteça algum incidente no sistema ou com sua conexão, feche a página, abra novamente o navegador e acesse o link Acompanhe sua Inscrição em nosso site.</p>
                <p>Para ter acesso a prova você deverá realizar o aceite ao Termo de Responsabilidade que será disponibilizado na próxima etapa.</p>
            </nav>

            <div class="container-prova topo-prova topo-estatico">
                <div class="row-prova">
                    <div class="col-7 col-4-sm">
                        <a href="#" class="logo-vestibulares-verde-menor align-left"></a>
                    </div>
                    <div class="col-5 col-8-sm" id="startValuesAndTargetExample">
                        <div class="processo-timer">
                            <span class="timer">
                                <p class="triangle-border right top" id="mensagem30">
                                    <span class="regulamento-fechar"></span>
                                    <asset:image src="prova/atencao.png"/> <strong>Atenção!</strong> Restam <b>30 minutos</b> para terminar a redação!
                                </p>
                                <p class="triangle-border right top" id="mensagem05">
                                    <span class="regulamento-fechar"></span>
                                    <asset:image src="prova/perigo.png"/> <strong>Atenção!</strong> Restam <b>05 minutos</b> para terminar a redação!
                                </p>
                                <asset:image src="prova/circular-clock-green.svg" class="hidden-sm" id="icone-timer" />
                                <asset:image src="prova/circular-clock-yellow.svg" class="hidden-sm" id="icone-timer2" />
                                <asset:image src="prova/circular-clock-orange.svg" class="hidden-sm" id="icone-timer3" />
                            </span>
                            <span class="titulo-timer">
                                <span class="show-sm">
                                    <asset:image src="prova/hora-menor-green.png" id="icone-timer-menor"/>
                                    <asset:image src="prova/hora-menor-green.png" id="icone-timer-menor2"/>
                                    <asset:image src="prova/hora-menor-green.png" id="icone-timer-menor2"/>
                                </span>
                                Tempo restante de prova:
                            </span>
                            <div class="values"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-prova bloco-principal bloco-prova-online">
                <div class="row-prova">
                    <div class="col-7 col-12-sm">
                        <h2>Desenvolvimento da redação</h2>
                    </div>
                    <div class="col-5 col-12-sm">
                        <a href="#" class="btn btn-regulamento" id="showLeft"><p>Ver regulamento</p></a>
                    </div>
                </div>
                <div class="row-prova">
                    <div class="col-12 prova-tema-acordeon">
                        <a href="#">
                            <span class="seta">
                                <asset:image src="prova/seta.png" />
                                <asset:image src="prova/seta-ativo.png" style="display:none" />
                            </span>
                            <strong>Tema </strong>
                        </a>
                    </div>
                    <div class="col-12 prova-tema-acordeon-conteudo">
                        <g:if test="${tema.arquivo}">
                            <p>${tema.arquivo}</p>
                        </g:if>
                        <p class="justify">
                            ${tema.descricao.encodeAsRaw()}
                        </p>
                    </div>
                </div>
                <div class="row-prova questoes-prova-online">
                    <div class="col-7">
                        <input type="hidden" name="inscricaoProvaOnlineId" value="${inscricaoProvaOnline?.id}">
                        <input type="text" name="tituloProva" required placeholder="Título da redação">
                        <label for="tituloProva" class="label-titulo">Insira o título da sua redação</label>
                    </div>
                </div>
                <div class="row-prova">
                    <div class="col-12">
                        <span class="align-right caracteres">2500/2500</span>
                        <textarea autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" name="textoProva" id="textoProva" maxlength="2500" required placeholder="Inicie a digitação da sua redação"></textarea>
                        <p class="avisoTempo"></p>
                    </div>
                </div>
                <div class="row-prova botoes-enviar-prova">
                    <div class="col-12 hidden-sm">
                        <g:link class="btn sair-redacao align-left" data-toggle="modal" data-target="#modalSair"><span class="sair"></span>Sair da redação</g:link>

                        <a href="#" class="btn enviar-redacao align-right disabled" data-toggle="modal" data-target="#modalConfirma"><strong>Enviar redação</strong></a>
                    </div>
                    <div class="col-12 show-sm">
                        <a href="#" class="btn enviar-redacao center disabled" data-toggle="modal" data-target="#modalConfirma"><strong>Enviar redação</strong></a>

                        <g:link class="btn sair-redacao align-left" data-toggle="modal" data-target="#modalSair"><span class="sair"></span>Sair da redação</g:link>
                    </div>
                </div>

                <div class="modal fade" id="modalConfirma" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header modal-header-sem-borda">
                                <h4 class="modal-title">Gostaria de finalizar sua redação?</h4>
                            </div>
                            <div class="modal-body">
                                <p class="center">
                                    Ao finalizar, você não poderá voltar para editar nenhuma informação.
                                </p>
                            </div>
                            <div class="modal-footer modal-footer-sem-borda center">
                                <button type="button" class="btn voltar-prova" data-dismiss="modal">Voltar</button>
                                <button type="submit" class="btn enviar-redacao center">Finalizar</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            </g:form>

        </main>

        <div class="modal fade" id="modalSair" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header modal-header-sem-borda">
                        <h4 class="modal-title">Tem certeza que gostaria de sair?</h4>
                        <span id="regulamento-fechar-modal" data-dismiss="modal" aria-label="Close"></span>
                    </div>
                    <div class="modal-body">
                        <g:if test="${quantidadeChancesRestantes >= 2}">
                            <p class="center">
                                Você tem apenas mais <b>${quantidadeChancesRestantes} CHANCE(S)</b> de fazer a redação online.
                            </p>
                        </g:if>
                        <g:else>
                            <p class="center">
                                Esta é sua <b>ULTIMA CHANCE</b> de fazer a redação online.
                            </p>
                        </g:else>
                    </div>
                    <div class="modal-footer modal-footer-sem-borda center">
                        <button type="button" class="btn voltar-prova" data-dismiss="modal">Não, voltar para a redação</button>
                        <g:link controller="provaOnline" action="instrucoesProvaOnline" id="${inscricao?.id.encode()}" class="btn fill continuar-prova">Sim, quero sair</g:link>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalFechaJanela" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header modal-header-sem-borda">
                        <h4 class="modal-title">Deseja mesmo atualizar esta tela?</h4>
                    </div>
                    <div class="modal-body">
                        <p class="center">
                            A tela será recarregada, e um novo tema e contagem serão estipulados.<br />
                            Você tem apenas mais <b>${quantidadeChancesRestantes} CHANCE(S)</b> de fazer a redação online.
                        </p>
                    </div>
                    <div class="modal-footer modal-footer-sem-borda center">
                        <button type="button" class="btn voltar-prova" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn fill continuar-prova">Continuar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalCopiarColar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header modal-header-sem-borda">
                        <h4 class="modal-title">Opa, eu vi o que você fez...</h4>
                    </div>
                    <div class="modal-body">
                        <p class="center">
                            É <strong class="text-danger">expressamente proibido copiar e colar</strong> conteúdo na avaliação.<br />
                            Você tem apenas mais <strong class="tentativasCopiarColar"></strong> tentativa(s) para terminar a avaliação.
                        </p>
                    </div>
                    <div class="modal-footer modal-footer-sem-borda center">
                        <button type="button" class="btn fill qtdeTentativas" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalCopiarColarErro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header modal-header-sem-borda">
                        <h4 class="modal-title">Bloqueado!</h4>
                    </div>
                    <div class="modal-body">
                        <p class="center">
                            Tentativas de copiar e colar excedidas...
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalFinalizacaoProva" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header modal-header-sem-borda">
                        <h4 class="modal-title">Tempo de prova finalizado!</h4>
                    </div>
                    <div class="modal-body">
                        <p class="center">
                            Estamos salvando sua prova, aguarde....
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <g:form controller="provaOnline" action="instrucoesProvaOnline">
            <input type="hidden" name="id" value="${inscricao.id}">
            <div class="modal fade" id="modal-screen-out" tabindex="-1" role="dialog" aria-labelledby="modalScreenOut" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header modal-header-sem-borda">
                            <h4 class="modal-title">Saída da área de redação!</h4>
                        </div>
                        <div class="modal-body">
                            <p class="center">
                                <g:if test="${quantidadeChancesRestantes <= 0}">
                                    Identificamos que você saiu da tela de prova - infelizmente esta era sua última chance.<br />
                                </g:if>
                                <g:else>
                                    Identificamos que você saiu da tela de prova, sua redação será reiniciada com um novo tema.<br />
                                    Você tem apenas mais <b>${quantidadeChancesRestantes} CHANCE(S)</b> de realizar a prova.
                                </g:else>
                            </p>
                        </div>
                        <div class="modal-footer modal-footer-sem-borda center">
                            <button type="submit" class="btn fill continuar-prova">Continuar</button>
                        </div>
                    </div>
                </div>
            </div>
        </g:form>

        <script>

            var provaFinalizada = false;
            var hostname = location.protocol + "//" + window.location.host + ((window.location.href.indexOf("/vestibular/") > -1) ? "/vestibular/": "/");

            $(function(){

                $('table').removeAttr('width');

                var acoesCopiarColar = 4;
                $('.tentativasCopiarColar').html(acoesCopiarColar);
                $('.qtdeTentativas').on('click',function(){
                    acoesCopiarColar = acoesCopiarColar - 1;
                    setTimeout(function(){
                        $('.tentativasCopiarColar').html(acoesCopiarColar);
                    }, 1500);
                });

                $(document).keydown(function(event) {
                    var ctrlDown = false,
                        ctrlKey = 17,
                        shftKey = 16,
                        insKey = 45,
                        escKey = 27,
                        cmdKey = 91,
                        f5Key = 116,
                        rKey = 82,
                        vKey = 86,
                        cKey = 67;

                    // acoes Ctrl C / V
                    if(
                        (event.ctrlKey || event.metaKey) && event.which == cKey ||
                        (event.ctrlKey || event.metaKey) && event.which == vKey ||
                        (event.ctrlKey || event.metaKey) && event.which == insKey ||
                        (event.shftKey || event.metaKey) && event.which == insKey
                    ){
                        // console.log('ctrlCV');
                        // chamada para acao de numero de tentativas
                        if(acoesCopiarColar > 0){
                            $('#modalCopiarColar').modal('toggle');
                        } else {
                            acoesCopiarColar = 0;
                            $('#modalCopiarColarErro').modal('toggle');
                            // chama evento de bloqueio
                        }
                        return false;
                    }

                    // acoes F5
                    if(
                        (event.ctrlKey || event.metaKey) && event.which == f5Key ||
                        (event.ctrlKey || event.metaKey) && event.which == rKey  ||
                        event.which == f5Key
                    ){
                        // console.log('f5');
                        $('#modalFechaJanela').modal('toggle');

                        $('.voltar-prova').on('click',function(){
                            $('#modalFechaJanela').modal('toggle');
                        });

                        $('.continuar-prova').on('click',function(){
                            // chama evento de refresh
                            location.reload();
                        });
                        return false;
                    }

                    // acoes ESC
                    if(event.which == escKey ){
                        console.log('esc pressionado');
                        event.stopImmediatePropagation();
                        return false;
                    }
                });

                // remover mouse right-button click
                document.oncontextmenu = document.body.oncontextmenu = function() {
                    return false;
                }

                $(".prova-tema-acordeon a").click(function() {
                    $(this).toggleClass('active');
                    $(this).find('.seta img').toggle();
                    $('.prova-tema-acordeon-conteudo').slideToggle("fast");
                    return false;
                });

                $('.sair-redacao').on('click',function(){
                    $('#modalSair').modal('toggle');
                });

                $('.enviar-redacao').on('click',function(){
                    $('#modalConfirma').modal('toggle');
                });

                $("#textoProva").on("input keyup", function () {
                    var limite = parseInt($(this).attr('maxlength'));
                    var caracteresDigitados = $(this).val().length;
                    var caracteresRestantes = limite - caracteresDigitados;

                    $(".caracteres").text(caracteresRestantes+'/'+limite);
                });
            });

            var timer = new Timer();
            timer.start({countdown: true, precision: 'seconds', startValues: { seconds: ${duracaoProvaOnline}} });
            // timer.start({countdown: true, precision: 'seconds', startValues: { seconds: 3600 }});
            
            $('#startValuesAndTargetExample .values').html(timer.getTimeValues().toString());
            timer.addEventListener('secondsUpdated', function (e) {
                $('#startValuesAndTargetExample .values').html(timer.getTimeValues().toString());

                if(timer.getTimeValues().toString() == '00:30:00'){
                // if(timer.getTimeValues().toString() == '00:00:55'){
                    $('.avisoTempo').html('<asset:image src="prova/atencao.png"/> <strong>Atenção!</strong> Restam <b>30 minutos</b> para terminar a redação!').addClass('atencao');
                    $('#icone-timer').hide();
                    $('#icone-timer2').show();
                    $('#icone-timer-menor').hide();
                    $('#icone-timer-menor2').show();
                    $('#mensagem30').toggle();
                    $('textarea').addClass('atencao');
                }
                if(timer.getTimeValues().toString() == '00:05:00'){
                // if(timer.getTimeValues().toString() == '00:00:50'){
                    $('.avisoTempo').html('<asset:image src="prova/perigo.png"/> <strong>Atenção!</strong> Restam <b>05 minutos</b> para terminar a redação!').addClass('perigo');
                    $('#icone-timer, #icone-timer2').hide();
                    $('#icone-timer3').show();
                    $('#icone-timer-menor, #icone-timer-menor2').hide();
                    $('#icone-timer-menor3').show();
                    $('#mensagem30').remove();
                    $('#mensagem05').toggle();
                    $('textarea').addClass('perigo');
                }
            });

            $('#mensagem30, #mensagem05').on('click',function(){
                $(this).toggle();
            });

            timer.addEventListener('targetAchieved', function (e) {
                $('#startValuesAndTargetExample .progress_bar').html('TEMPO DE PROVA FINALIZADO!!');
                $('.enviar-redacao').addClass('disabled').attr('disabled');
                $('#modalFinalizacaoProva').modal('toggle');
                provaFinalizada = true;
                setTimeout(function(){ $("#save-prova-auto").submit() }, 5000);
            });

            $(window).blur(function() {
                //do something
                if(!provaFinalizada)
                    $('#modal-screen-out').modal('show');
            });

            $('input[type="text"],textarea').on('blur change keyup',function(){
                var valor = $(this).val();
                if (valor.length < 1){
                    $(this).addClass('error');
                    $(this).next('label.error').remove();
                    $(this).after('<label class="error">Preenchimento obrigatório.</label>');
                } else {
                    $(this).removeClass('error');
                    $(this).next('label.error').remove();
                }

                if($('input[name="tituloProva"]').val() != "" && $('textarea[name="textoProva"]').val() != ""){
                    $('.enviar-redacao').removeClass('disabled');
                } else {
                    $('.enviar-redacao').addClass('disabled');
                }
            });

        </script>
    </body>
</html>