<html>
	<head>
		<meta name="layout" content="prova-online" />
	</head>
	<body class="prova-online">
		<main>

			<div class="center">
				<a href="#" class="logo-vestibulares-verde"></a>
			</div>

			<div class="container-prova bloco-principal">
				<div class="row-prova">
					<div class="col-2 col-12-sm center logo-apresentacao-prova">
						<g:if test="${!poloMarca.marca.id}">
							<asset:image src="logos/logo_marca_kroton.png" alt="LOGO MARCA"/>
						</g:if>
						<g:else>
							<asset:image src="logos/logo_marca_${poloMarca?.marca?.id}.gif" alt="LOGO MARCA"/>
						</g:else>
					</div>
					<div class="col-10 col-12-sm inicio-apresentacao">
						<p>
							<b>${inscricao?.nome}</b>, chegou o momento de realizar a sua prova on-line! </br>
							Você se inscreveu para o curso de <b>${inscricaoOpcao?.ofertaPolo?.oferta?.modulo.matriz.curso.nome}</b> na <b>${poloMarca?.marca?.descricao}</b>.</br>
							Antes de iniciar, leia com atenção as regras abaixo:
						</p>
					</div>
				</div>
			</div>

			<div class="container-prova bloco-principal hidden-sm">
				<div class="row-prova">
					<div class="col-12">
						<h2>Instruções para a prova on-line</h2>
					</div>
				</div>
				<div class="row-prova instrucoes instrucoes-topo">
					<div class="col-6">
						<span class="iconesInstrucoes"><asset:image src="prova/notes-square-outlined-symbol-of-interface-with-text-lines.svg" /></span>
						<p>
							A prova consiste em uma redação em língua Portuguesa.
						</p>
					</div>
					<div class="col-6">
						<span class="iconesInstrucoes"><asset:image src="prova/ico-copy.svg" /></span>
						<p>
							A opção de copiar e colar texto está bloqueada na redação.
						</p>
					</div>
				</div>
				<div class="row-prova instrucoes">
					<div class="col-6">
						<span class="iconesInstrucoes"><asset:image src="prova/ico-tempo.svg" /></span>
						<p>
							A prova tem um cronômetro que controla o tempo e encerra a aplicação ao limite ser atingido.
						</p>
					</div>
					<div class="col-6">
						<span class="iconesInstrucoes"><asset:image src="prova/ico-font.svg" /></span>
						<p>
							A correção ortográfica do texto digitado está desabilitada.
						</p>
					</div>
				</div>
				<div class="row-prova instrucoes">
					<div class="col-6">
						<span class="iconesInstrucoes"><asset:image src="prova/warning-weather-interface-outlined-symbol.svg" class="icone-warning" /></span>
						<p>
							Não há como pausar, reiniciar ou retomar a prova quando iniciar. Então preste atenção na sua conexão, para que não seja interrompida.
						</p>
					</div>
					<div class="col-6">
						<span class="iconesInstrucoes"><asset:image src="prova/browser-visualization.svg" /></span>
						<p>
							Caso saia da aba do navegador, sua redação será recomeçada com um novo tema.
						</p>
					</div>
				</div>
				<div class="row-prova instrucoes">
					<div class="col-6">
						<span class="iconesInstrucoes"><asset:image src="prova/silence.svg" /></span>
						<p>
							Para que você tenha um melhor desempenho, realize a prova em um local tranquilo, sem ruídos ou distrações.
						</p>
					</div>
					<div class="col-6">
						<span class="iconesInstrucoes"><asset:image src="prova/wifi.svg" class="icone-warning" /></span>
						<p>
							Caso aconteça algum incidente no sistema ou com sua conexão, feche a página, abra novamente o navegador e acesse o link “Acompanhe sua Inscrição” em nosso site.
						</p>
					</div>
				</div>
				<div class="row-prova instrucoes">
					<div class="col-6">
						<span class="iconesInstrucoes"><asset:image src="prova/copy-file.svg" /></span>
						<p>
							Se identificado plágio no texto digitado o mesmo será desconsiderado e a nota da redação será zero, desclassificando o candidato.
						</p>
					</div>
				</div>
				<div class="bloco-links">
					<div class="row-prova center">
						<a href="/" class="btn">Sair</a>
						<g:link controller="provaOnline" action="aceiteProvaOnline" id="${inscricao.id.encode()}" class="btn fill">Ok, entendi</g:link>
					</div>
				</div>
			</div>
			
			<div class="container-prova bloco-principal show-sm">
				<div class="row-prova">
					<div class="col-12">
						<h2>Instruções para a prova on-line</h2>
					</div>
				</div>
				<div class="swiper-container">
					<div class="swiper-wrapper">
						<div class="swiper-slide instrucoes instrucoes-topo">
							<span class="iconesInstrucoes"><asset:image src="prova/notes-square-outlined-symbol-of-interface-with-text-lines.svg" /></span>
							<div class="col-12-sm instrucoes instrucoes-topo">
								<p>
									A prova consiste em uma redação em língua Portuguesa.
								</p>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="col-12-sm instrucoes instrucoes-topo">
								<span class="iconesInstrucoes"><asset:image src="prova/ico-copy.svg" /></span>
								<p>
									A opção de copiar e colar texto está bloqueada na redação.
								</p>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="col-12-sm instrucoes instrucoes-topo">
								<span class="iconesInstrucoes"><asset:image src="prova/ico-tempo.svg" /></span>
								<p>
									A prova tem um cronômetro que controla o tempo e encerra a aplicação ao limite ser atingido.
								</p>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="col-12-sm instrucoes instrucoes-topo">
								<span class="iconesInstrucoes"><asset:image src="prova/ico-font.svg" /></span>
								<p>
									A correção ortográfica do texto digitado está desabilitada.
								</p>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="col-12-sm instrucoes instrucoes-topo">
								<span class="iconesInstrucoes"><asset:image src="prova/warning-weather-interface-outlined-symbol.svg" class="icone-warning" /></span>
								<p>
									Não há como pausar, reiniciar ou retomar a prova quando iniciar. Então preste atenção na sua conexão, para que não seja interrompida.
								</p>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="col-12-sm instrucoes instrucoes-topo">
								<span class="iconesInstrucoes"><asset:image src="prova/browser-visualization.svg" /></span>
								<p>
									Caso saia da aba do navegador, sua redação será recomeçada com um novo tema.
								</p>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="col-12-sm instrucoes instrucoes-topo">
								<span class="iconesInstrucoes"><asset:image src="prova/silence.svg" /></span>
								<p>
									Para que você tenha um melhor desempenho, realize a prova em um local tranquilo, sem ruídos ou distrações.
								</p>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="col-12-sm instrucoes instrucoes-topo">
								<span class="iconesInstrucoes"><asset:image src="prova/wifi.svg" class="icone-warning" /></span>
								<p>
									Caso aconteça algum incidente no sistema ou com sua conexão, feche a página, abra novamente o navegador e acesse o link “Acompanhe sua Inscrição” em nosso site.
								</p>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="col-12-sm instrucoes instrucoes-topo">
								<span class="iconesInstrucoes"><asset:image src="prova/copy-file.svg" /></span>
								<p>
									Se identificado plágio no texto digitado o mesmo será desconsiderado e a nota da redação será zero, desclassificando o candidato.
								</p>
							</div>
						</div>
					</div>
					<!-- Add Pagination -->
					<div class="swiper-pagination"></div>
					<!-- Add Arrows -->
					<div class="swiper-button-next"></div>
					<div class="swiper-button-prev"></div>
				</div>
				<div class="bloco-links">
					<div class="row-prova center">
						<a href="" class="btn">Sair</a>
						<g:link controller="provaOnline" action="aceiteProvaOnline" id="${inscricao.id}" class="btn fill">Ok, entendi</g:link>
					</div>
				</div>
			</div>

		</main>

		%{-- <g:link controller="provaOnline" action="aceiteProvaOnline" id="${inscricao.id}" >
			aceite prova online
		</g:link> --}%

	</body>
</html>