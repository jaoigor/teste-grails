<html>
	<head>
		<meta name="layout" content="prova-online"/>
		<meta http-equiv="refresh" content="3; url=/prova-online" />
	</head>
	<body class="prova-online">
		<main>
			<div class="container-prova">
				<div class="col-12"  id="aceiteScroll">
				<g:if test="${flash.error}">
					<div class="alert alert-danger center">
						<h2><i class="fa fa-times-circle"></i> Erro</h2>
						<p>
							${flash.error}
						</p>
					</div>
					<div class="large-12 columns">
						<p class="box error"></p>
					</div>
				</g:if>
					<p class="center">
						Se você não for redirecionado automaticamente clique <a href="/prova-online">aqui</a>.
					</p>
				</div>
			</div>
		</main>
	</body>
</html>