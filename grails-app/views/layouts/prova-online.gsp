<%@page import="grails.util.Environment"%>

<!DOCTYPE html>

<!--[if lt IE 7 ]> <html lang="pt-br" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="pt-br" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="pt-br" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="pt-br" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="pt-br" class="no-js"><!--<![endif]-->

	<head>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="description" content="Conheça todos os cursos de educação a distância para graduação digital e inscreva-se no vestibular. Inscrições Abertas!" />

		<title>
			<g:layoutTitle default="Inscreva-se "/>Prova Online | Vestibular Unopar Ead
		</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<asset:link rel="shortcut icon" href="icon/favicon.ico" type="image/x-icon"/>

		<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,700|Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
	    <asset:stylesheet src="provaOnline.less" />

		<asset:javascript src="jquery.js" />
		<asset:javascript src="bootstrap.js" />
		<asset:javascript src="modernizr.js" />
		<asset:javascript src="prova/easytimer.js" />
		<asset:javascript src="prova/classie.js" />
		<asset:javascript src="prova/swiper.min.js" />
		<asset:javascript src="prova/jquery.nicescroll.min.js" />
		<asset:javascript src="bootstrap.js" />

		%{-- <asset:javascript src="prova/jquery.nicescroll.iframehelper.min.js" /> --}%
		<g:layoutHead />

		<script type="text/javascript" src="https://tags.bluekai.com/site/52936?ret=js&limit=1"> </script>
		<script>
			(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-MCWRNM');
		</script>

	</head>
	<body class="prova-online">

		<!-- Google Tag Manager -->
		<noscript>
			<iframe src="//www.googletagmanager.com/ns.html?id=GTM-MCWRNM" height="0" width="0" style="display:none;visibility:hidden"></iframe>
		</noscript>
		<!-- End Google Tag Manager -->

		<g:if test="${Environment.current == Environment.DEVELOPMENT}">
			<div class="box error homologacao" role="alert">Ambiente de Homologação</div>
		</g:if>
	        	
		<g:layoutBody />

		<div class="scroll-top"></div>

		<footer class="prova-online-footer">
			<a href="#" class="logo-vestibulares-footer"></a> &copy; Copyright 2018 Vestibulares. Todos os direitos reservados.
		</footer>
		
		<script>
			$(function(){
				$("#aceiteScroll, #cbp-spmenu-s1").niceScroll({
					cursorcolor:"#009788",
					cursorwidth:"6px",
					cursorborder:"1px solid #009788",
					background:"#f4f4f4",
					cursorborderradius:100,
					nativeparentscrolling: true
				});
				var nice = $('#aceiteScroll').getNiceScroll()[0];
				$('#aceiteScroll').bind("scroll",function(){
					if((nice.scrollvaluemax - nice.scroll.y) < 20) {
						$('.aceitaTermo').removeClass('disabled').removeAttr('disabled');
					}
				});
				
				// Scroll to top
			    // ----------------------------------------------------------------

			    var $wrapper = $('.wrapper');
			    $wrapper.append($("<div class='scroll-top'></div>"));

			    var $scrollbtn = $('.scroll-top');

			    $(document).on('ready scroll', function () {
			        var docScrollTop = $(document).scrollTop(),
			            docScrollBottom = $(window).scrollTop() + $(window).height() == $(document).height();

			        if (docScrollTop >= 150) {
			            $scrollbtn.addClass('visible');
			        } else {
			            $scrollbtn.removeClass('visible');
			        }
			        if (docScrollBottom) {
			            $scrollbtn.addClass('active');
			        }
			        else {
			            $scrollbtn.removeClass('active');
			        }
			    });

			    $scrollbtn.on('click', function () {
			        $('html,body').animate({
			            scrollTop: $('body').offset().top
			        }, 1000);
			        return false;
			    });
			});

			var swiper = new Swiper('.swiper-container', {
				pagination: {
					el: '.swiper-pagination',
					type: 'fraction',
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
			});

			// menu regulamento
			$('#regulamento-fechar').on('click',function(){
				$('#showLeft').trigger('click');
			});
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				bg = document.getElementById( 'bg-menu-regulamento' ),
				body = document.body;

			showLeft.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				classie.toggle( bg, 'active' );
				disableOther( 'showLeft' );
			};

			function disableOther( button ) {
				if( button !== 'showLeft' ) {
					classie.toggle( showLeft, 'disabled' );
				}
			}
		</script>
	    <asset:deferredScripts/>	
		
	</body>

</html>