<html>

<head>
    <meta name="layout" content="prova-online"/>
    <title>Erro 500</title>
</head>

<body class="prova-online">

<div class="container-prova bloco-principal">

    <div class="row-prova">
        <div class="col-12" id="aceiteScroll">
            <h3 class="page-title">Erro 500</h3>
            <p>Por gentileza, tente novamente mais tarde. Caso o problema persista, entre em contato pelo <a
                    href="mailto:colaborar-vestibular@kroton.com.br">aqui</a>.</p>
        </div>

    </div>
</div>
</body>
</html>