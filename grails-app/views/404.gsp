<html>

<head>
    <meta name="layout" content="prova-online"/>
    <title>404 Página não encontrada</title>
</head>

<body class="prova-online">

<div class="container-prova bloco-principal">

    <div class="row-prova">
        <div class="col-12" id="aceiteScroll">
            <h3 class="page-title">404 - Página não Encontrada</h3>
            <p class="color-white">A página que você está procurando não existe, verifique se o endereço digitado esta correto.</p>
        </div>

    </div>
</div>
</body>
</html>