package br.unopar.domains.persistence

import java.sql.*

import org.hibernate.HibernateException
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.StandardBasicTypes

/**
 *
 * @author volnei
 */
class SNUserType implements org.hibernate.usertype.EnhancedUserType {

	public int[] sqlTypes() {
		[StandardBasicTypes.YES_NO.sqlType()]
	}

	private Class targetClass;

	public void setParameterValues(Properties params) {
		String targetClassName = params.getProperty("targetClass")
		try {
			targetClass = Class.forName(targetClassName);
		} catch (ClassNotFoundException e) {
			throw new HibernateException("Class " + targetClassName + " not found ", e)
		}
	}

	public Class returnedClass() {
		return targetClass;
	}

	public boolean isMutable() {
		return false;
	}

	public Object deepCopy(Object value) {
		return value;
	}

	public Serializable disassemble(Object value) {
		return (Serializable) value;
	}

	public Object assemble(Serializable cached, Object owner) {
		return cached;
	}

	public Object replace(Object original, Object target, Object owner) {
		return original;
	}

	public boolean equals(Object x, Object y) {
		if (x == y)
			return true;
		if (x == null || y == null)
			return false;
		return x.equals(y);
	}

	public int hashCode(Object x) {
		return x.hashCode();
	}

	public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor s, Object owner) throws SQLException {
		String value = rs.getString(names[0]);
		if ("S".equals(value))
			return true;
		else
			return false;
	}

	public void nullSafeSet(PreparedStatement ps, Object value, int index, SessionImplementor s) throws HibernateException, SQLException {
		if (value == null) {
			ps.setNull(index, StandardBasicTypes.YES_NO.sqlType());
		} else {
			if((Boolean)value) {
				ps.setString(index, "S");
			} else {
				ps.setString(index, "N");
			}
		}
	}

	@Override
	public Object fromXMLString(String arg0) {
		return null;
	}

	@Override
	public String objectToSQLString(Object arg0) {
		return arg0.toString();
	}

	@Override
	public String toXMLString(Object arg0) {
		return arg0.toString();
	}
}