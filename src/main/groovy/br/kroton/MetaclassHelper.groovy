package br.kroton

import java.text.Normalizer
import org.apache.commons.lang.StringUtils
import org.apache.commons.lang.WordUtils

import javax.servlet.http.HttpServletRequest

public class MetaclassHelper {

    public MetaclassHelper() {

        String.metaClass.slug = { ->

            def s = delegate.toLowerCase()

            s = Normalizer.normalize(s, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").trim()
            s = s.replaceAll(/[^a-z0-9\s-]/, "").replaceAll(/\s+/, " ").trim()

            if (s.length() > 45) {
                s = s.substring(0, 45).trim()
            }

            s = s.replaceAll(/\s/, "-")
            s = s.replaceAll("---", "-")
            return s
        }

		HttpServletRequest.metaClass.isXhr = {->
			'XMLHttpRequest' == delegate.getHeader('X-Requested-With')
	   }

        String.metaClass.capitalize = { ->
            return StringUtils.capitalize(delegate.toLowerCase())
        }

        String.metaClass.justNumber = { ->
            return delegate.replaceAll("\\D+","")
        }

        String.metaClass.capitalizeAll = { ->
            return "${WordUtils.capitalizeFully(delegate.toLowerCase())} "
                    .replace(" A "," a ")
                    .replace(" E "," e ")
                    .replace(" O "," o ")
                    .replace(" Da "," da ")
                    .replace(" De "," de ")
                    .replace(" Do "," do ")
                    .replace(" Em "," em ")
                    .replace(" Para "," para ")
                    .replace(" Pra "," pra ")
                    .replace("Ii "," II ")
                    .replace("Iii "," III ")
                    .replace("Iv "," IV ")
                    .replace("Vi "," VI ")
                    .replace("Vii "," VII ")
                    .replace("Viii "," VIII ")
                    .replace("Ix "," IX ").trim()
        }

        String.metaClass.removePrefixSuperior = { ->
            return delegate.replace("SUPERIOR DE ","").replace("TECNOLOGIA EM ","").replace("- HABILITAÇÃO: LICENCIATURA EM LÍNGUA PORTUGUESA E RESPECTIVAS LITERATURAS","")
        }
		
        String.metaClass.decodeUrlImage = { ->
            def s = delegate.getBytes("UTF-8");
            return new String(s, "ISO-8859-1")
        }

        String.metaClass.removeAccent = { ->
            return Normalizer.normalize(delegate, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "")
        }

        String.metaClass.encode = { ->
            delegate.encodeAsBase64()
        }

        String.metaClass.decode = { ->
            if (delegate.endsWith("=")) {
                return new String(delegate.decodeBase64())
            } else {
                def hash = delegate.split("-")

                if (hash.size() != 3) {
                    return ""
                }

                def first  = (hash[0] as Integer)/2
                def second = (hash[1] as Integer)/4
                def third  = (hash[2] as Integer)/5
                try {
                    return "${first}${second}${third}" as Integer
                } catch (e) {
                    return ""
                }
            }

        }

    }

}